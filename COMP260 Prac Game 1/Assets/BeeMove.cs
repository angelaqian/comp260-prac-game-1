﻿using UnityEngine;
using System.Collections;

public class BeeMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		// heading = heading.Rotate(angle);
		PlayerMove player = (PlayerMove) FindObjectOfType<PlayerMove>();
		target1 = player.transform;
	}

	//public float minSpeed, maxSpeed;


//	public float speed = 4.0f;
//	public float turnSpeed = 180.0f;
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;
	private float speed;
	private float turnspeed;

//	public Transform target1;
//	public Transform target2;
	private Transform target;
//	private Transform currentTarget;
//	public Vector2 heading = Vector3.right;
	private Vector2 heading;
	public ParticleSystem Explosion;
	
	// Update is called once per frame
	void Update () {
		// get the vector from the bee to the target
		//Vector2 direction = currentTarget.position - transform.position;

		float angle = turnSpeed * Time.deltaTime;
		Vector2 distance1 = target1.position - transform.position;
		Vector2 distance2 = target2.position - transform.position;

		if (distance1.magnitude < distance2.magnitude) {
			currentTarget = target1;
		}
		else {
			currentTarget = target2;
		}

		//calculate how much to turn per frame

		Vector2 distance = currentTarget.position - transform.position;

		if (distance.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate (angle);
		} else {
			//target on right, rotate clockwise
			heading = heading.Rotate (-angle);
		}

		// turn left or right
		// if (direction.IsOnLeft (heading)) {
			// target on left, rotate anticlockwise
		// 	heading = heading.Rotate (angle);
		// } else {
			//target on right, rotate clockwise
		// 	heading = heading.Rotate (-angle);
		// }
			
		transform.Translate (heading * speed * Time.deltaTime);
	
	}
}
