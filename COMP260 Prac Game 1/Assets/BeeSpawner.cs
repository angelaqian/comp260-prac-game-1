﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	public BeeMove beePrefab;
	public PlayerMove player;
	public int nBees = 50;
	public float xMin, yMin;
	public float width, height;


	// Use this for initialization
	void Start () {
		for (int i = 0; i < nBees; i++) {
			BeeMove bee = Instantiate (beePrefab);

			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			bee.transform.position = new Vector2 (x, y);
		}
//		bee.target = player;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
