﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	public Vector2 move;
	public Vector2 velocity;
	public float maxSpeed = 5.0f;
	public bool usingArrowKeys = true;
	
	// Update is called once per frame
	void Update () {

		if (usingArrowKeys) {

			// get the input values
			Vector2 direction;
			direction.x = Input.GetAxis ("HorizontalP1");
			direction.y = Input.GetAxis ("VerticalP1");

			// scale the veolocity by the frame duration
			Vector2 velocity = direction * maxSpeed;

			// move the object
			transform.Translate (velocity * Time.deltaTime);
		} else {
			Vector2 direction;
			direction.x = Input.GetAxis ("HorizontalP2");
			direction.y = Input.GetAxis ("VerticalP2");
			Vector2 velocity = direction * maxSpeed;
			transform.Translate (velocity * Time.deltaTime);
		}
	}
}
